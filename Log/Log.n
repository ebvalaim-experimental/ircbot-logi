using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;

using Nemerle.Collections;
using Nemerle.Linq;

class Log : IEnumerable
{
	mutable lines : List[LogLine];

	public this()
	{
		lines = List();
	}
	
	private indexToInsert(date : DateTime) : int
	{
		def _indexToInsert(date : DateTime, start : int, end : int) : int
		{
			match(end-start)
			{
				| 0 => end
				| 1 => if(date >= lines[start].date) end else start;
				| _ => {
					def mid = (start + end) / 2;
					def midVal = lines[mid].date;
					if(date >= midVal)
						_indexToInsert(date, mid, end)
					else
						_indexToInsert(date, start, mid)
				}
			}
		}
		
		_indexToInsert(date, 0, lines.Count);
	}
		

	public insert(line : LogLine) : void
	{
		def i = indexToInsert(line.date);
		def range = 3;
		def imin = if (i > range) i - range else 0;
		def imax = if (i + range < lines.Count) i + range else lines.Count;
		when(lines.GetRange(imin, imax-imin).TrueForAll((x) => x.author != line.author || x.message != line.message)) {
			if(i < lines.Count)
				lines.Insert(i, line);
			else
				lines.Add(line);
		}
	}
	
	public insert(lines : List[LogLine]) : void
	{
		lines.Iter((line) => this.insert(line));
	}

	public Item[i : int] : LogLine
	{
		get { lines[i] }
	}

	public Count : int { get { lines.Count } }

	public GetEnumerator() : IEnumerator
	{
		lines.GetEnumerator();
	}
	
	private genPath(baseDir : string, date : DateTime) : string
	{
		mutable dir = baseDir + "\\" + date.Year.ToString("D4");
		unless(Directory.Exists(dir))
			_ = Directory.CreateDirectory(dir);
		dir = dir + "\\" + date.Month.ToString("D2");
		unless(Directory.Exists(dir))
			_ = Directory.CreateDirectory(dir);
			
		dir + "\\" + date.Day.ToString("D2") + ".txt"
	}
	
	public save(baseDir : string) : void
	{
		unless(Directory.Exists(baseDir))
			_ = Directory.CreateDirectory(baseDir);
			
		mutable oldDate : DateTime? = null;
		mutable dayPassed = false;
		mutable currentDate = lines[0].date.Date;
		
		foreach(line in lines)
		{
			def dateDiff = if (oldDate.HasValue)
								line.date.Subtract(oldDate.Value)
							else
								TimeSpan(0, 0, 1);
								
			when(oldDate.HasValue && line.date.Date > oldDate.Value.Date)
				dayPassed = true;
				
			when(dayPassed && (dateDiff.TotalSeconds > 14400 || line.date.Hour >= 6))
			{
				Console.WriteLine(line.date.ToString("yyyy-MM-dd"));
				currentDate = line.date.Date;
				dayPassed = false;
			}
			
			def file = genPath(baseDir, currentDate);
			File.AppendAllLines(file, [line.format()]);
			
			oldDate = line.date;
		}
	}
}