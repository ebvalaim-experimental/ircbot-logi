using System;
using System.IO;
using System.Collections.Generic;

using Nemerle.Collections;

class LogLine
{
	public date : DateTime;
	public author : string;
	public message : string;

	public this(date : DateTime, author : string, message : string)
	{
		this.date = date;
		this.author = author;
		this.message = message.TrimEnd(' ');
	}
	
	public format() : string
	{
		def insertAuthor = if(author == null || author == "") " " else $" <$author> ";
		def dateString = date.ToString("(yyyy-MM-dd HH:mm:ss)");
		$"$dateString$insertAuthor$message";
	}
}