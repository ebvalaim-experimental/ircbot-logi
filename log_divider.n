using System;
using System.IO;
using System.Collections.Generic;

using Nemerle.Collections;

module Program 
{
	listDir(path : string) : List[string]
	{
		def dirs = Directory.GetDirectories(path);
		def files = List(dirs.Map((x) => listDir(x)));
		def result = List(Directory.GetFiles(path).Filter((x) => x.EndsWith(".txt") || x.EndsWith(".html")));
		result.AddRange(files.Flatten());
		result
	}

	readLogs(path : string) : void
	{
		def log = Log();
		listDir(path).Iter( (file) => log.insert(ParserFactory.parse(file)) );
		Console.WriteLine(log.Count);
		log.save("new_logs");
	}

	Main(argv : array[string]) : void
	{
		match(argv.Length)
		{
			| 1 => readLogs(argv[0])
			| _ => Console.WriteLine("Wymagany parametr: katalog ze starymi logami")
		}
	}
}