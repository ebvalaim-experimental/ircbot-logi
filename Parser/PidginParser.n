﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;

using Nemerle.Collections;
using Nemerle.Text;

class PidginParser : ILogParser
{
	mutable date : DateTime = DateTime.Now;

	public parseFile(path : string) : List[LogLine]
	{
		def lines = File.ReadAllLines(path);
		def result = List();
		mutable previousH = -1;
		
		def newDate(h : int, m : int, s : int) : void
		{
			date = if (h < previousH)
							date.AddDays(1.0)
						else
							date;
			date = DateTime(date.Year, date.Month, date.Day, h, m, s);
			previousH = h;
		}

		lines.Iter((line) => regexp match(line) {
			| @"^<html><head>.*<title>(?<title>Conversation with [^ ]+ at [^ ]+, (?<D:int>\d+) (?<Mstr>[^ ]+) (?<Y:int>\d\d\d\d), (?<h:int>\d\d):(?<m:int>\d\d):(?<s:int>\d\d) on .*)</title>.*" => { 
				def months = List(["sty", "lut", "mar", "kwi", "maj", "cze", "lip", "sie", "wrz", "paź", "lis", "gru"]);
				def M = months.IndexOf(Mstr) + 1;
				date = DateTime(Y, M, D, h, m, s);
				previousH = h;
				result.Add(LogLine(date, "", $"** $title **"))
			}
			| "^<font color=\"#[0-9A-F]{6}\"><font size=\"2\">\\((?<h:int>\\d\\d):(?<m:int>\\d\\d):(?<s:int>\\d\\d)\\)</font> <b>(?<nick>.+)</b></font> (?<message>.*)<br/>" => {
				newDate(h, m, s);
				result.Add(if(nick.EndsWith(":")) LogLine(date, nick.Remove(nick.Length-1, 1), message) else LogLine(date, "", $"$nick $message"));
			}
			| "^<font size=\"2\">\\((?<h:int>\\d\\d):(?<m:int>\\d\\d):(?<s:int>\\d\\d)\\)</font><b> (?<text>.*)</b><br/>" => {
				newDate(h, m, s);
				result.Add(LogLine(date, "", $"** $text **"));
			}
			| @"^</body></html>" => ()
			| _ => throw LogParseException(path + "\t" + line)
		});
		
		result;
	}
}