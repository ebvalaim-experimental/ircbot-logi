using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;

using Nemerle.Collections;
using Nemerle.Text;

class OldParser : ILogParser
{
	mutable date : DateTime = DateTime.Now;

	public parseFile(path : string) : List[LogLine]
	{
		def lines = File.ReadAllLines(path, Text.Encoding.Default);
		def result = List();
		mutable previousH = -1;

		lines.Iter((line) => regexp match(line) {
			| @"\*\*Start programu.+(?<D:int>\d\d)\.(?<M:int>\d\d)\.(?<Y:int>\d\d\d\d) (?<h:int>\d\d)\:(?<m:int>\d\d)\:(?<s:int>\d\d)" => { 
				date = DateTime(Y, M, D, h, m, s);
				previousH = h;
				result.Add(LogLine(date, "", line)) 
			}
			| @"\*\*.+" => result.Add(LogLine(date, "", line))
			| @"\((?<h:int>\d\d)\:(?<m:int>\d\d)\:(?<s:int>\d\d)\)(?<text>.*)" => {
				date = if (h < previousH)
							date.AddDays(1.0)
						else
							date;
				date = DateTime(date.Year, date.Month, date.Day, h, m, s);
				previousH = h;
				result.Add(regexp match(text)
				{
					| @"<(?<nick>[^>]+)> (?<message>.*)" => LogLine(date, nick, message)
					| _ => LogLine(date, "", text)
				})
			}
			| @"\s*"
			| @"\-*" => result.Add(LogLine(date, "", line))
			| _ => throw LogParseException(path + "\t" + line)
		});
		
		result;
	}
}