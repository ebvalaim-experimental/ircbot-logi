using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;

using Nemerle.Collections;

interface ILogParser
{
	parseFile(path : string) : List[LogLine];
}

class LogParseException : Exception
{
	public this(msg : string)
	{
		base(msg);
	}
}