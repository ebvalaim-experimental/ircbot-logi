using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;

using Nemerle.Collections;
using Nemerle.Text;

class QtParser : ILogParser
{
	public parseFile(path : string) : List[LogLine]
	{
		def lines = File.ReadAllLines(path);
		def result = List();
		
		lines.Iter((line) => regexp match(line) {
			| @"\((?<D:int>\d\d)\.(?<M:int>\d\d)\.(?<Y:int>\d\d\d\d), (?<h:int>\d\d)\:(?<m:int>\d\d)\:(?<s:int>\d\d)\)\s*(?<text>.*)" => {
				def date = DateTime(Y, M, D, h, m, s);
				result.Add(regexp match(text) {
					| @"<(?<nick>[^>]+)> (?<message>.*)" => LogLine(date, nick, message)
					| _ => LogLine(date, "", text)
				});
			}
			| @"\s*" => ()
			| _ => throw LogParseException(path + "\t" + line)
		});
		result
	}
}