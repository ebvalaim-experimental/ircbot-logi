using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;

using Nemerle.Collections;

class ParserFactory
{
	static parsers : list[Type] = [ typeof(OldParser), typeof(OldParser2), typeof(QtParser), typeof(PidginParser) ];

	public static parse(path : string) : List[LogLine]
	{
		def tryParse(parsersLeft)
		{
			| [] => List()
			| t :: tail => try {
					def parser : ILogParser = t.GetConstructors()[0].Invoke(null) :> ILogParser;
					parser.parseFile(path);
				}
				catch {
					| e => { Console.WriteLine(e.Message); tryParse(tail) }
				}
		}

		tryParse(parsers);
	}
}